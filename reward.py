import pygame
from random import randint
import os
EXEC_DIR = os.path.dirname(__file__)

class Reward(pygame.sprite.Sprite):
    """ This is the main reward class """
    def __init__(self, position):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load('reward.png')
        self.rect = self.image.get_rect()
        self.rect.topleft = position
        self.position = self.rect.topleft
        self.bites = 0
        self.whole_image = pygame.image.load('reward.png')
        self.bite1_image = pygame.image.load('reward_bite1.png')
        self.bite2_image = pygame.image.load('reward_bite2.png')

    def update(self):
        if self.bites == 1:
            self.image = self.bite1_image
        elif self.bites == 2:
            self.image = self.bite2_image
        elif self.bites == 3:
            self.image = self.whole_image
            self.rect.topleft = [randint(20, 400), randint(20, 400)]

