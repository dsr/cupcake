import sqlite3
import sys
import os
import platform



def replace_maccr(x):
    """Open text file and replace Mac CRs"""
    f = open(x, 'r')
    str = f.read()
    f.close()
    str = str.replace('\r', '\n')
    f_new = open(x, 'w')
    f_new.write(str)
    f_new.close()
    
def db_connect(command, path, db_name):
    """Create and connect to a sqlite database file and execute a command without variables"""
    db_path = path + db_name
    connect = sqlite3.connect(db_path)
    cursor = connect.cursor()
    data = cursor.execute(command)
    return data
    connect.commit()
    cursor.close()
    
def get_app_support_dir():
    """find the application support directory on mac os x"""
    user_sys = platform.system()
    user_name = os.getenv('LOGNAME')
    if user_sys == "Darwin":    
        app_support_dir = "/Users/%s/Library/Application Support/" % user_name
    elif user_sys == "Linux":
        app_support_dir = "/home/%s/" % user_name
    return app_support_dir
