import pygame

class Box(pygame.sprite.Sprite):
    def __init__(self, color, initial_position):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load('circles_blue.png')
        self.image = pygame.Surface(initial_position)
        self.image.fill(color)
        self.rect = self.image.get_rect()
        self.rect.topleft = initial_position
        self.next_update_time = 0
        self.move_right = True
        
    # def update(self, current_time, right):
    #         if self.next_update_time < current_time:
    #             if self.rect.right == right - 1:
    #                 self.move_right = False
    #             elif self.rect.left == 0:
    #                 self.move_right = True
    #                 
    #             if self.move_right:
    #                 self.rect.right += 1
    #             else:
    #                 self.rect.right -= 1
    #                 
    #             self.next_update_time = current_time + 10
    #             print str(self.rect.right)
            
        