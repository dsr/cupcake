import pygame
import os
DIR = os.path.dirname(__file__)

class Wall(pygame.sprite.Sprite):
    def __init__(self, start_position, type):
        pygame.sprite.Sprite.__init__(self)
        if type == 'horizontal':
            self.image = pygame.image.load('wall_h.png')
        elif type == 'vertical':
            self.image = pygame.image.load('wall_v.png')
        
        self.rect = self.image.get_rect()
        self.rect.topleft = start_position
        self.bottom = self.rect.bottom
        self.top = self.rect.top
        self.right = self.rect.right
        self.left = self.rect.left
        
    #def check_collision(self, wall_type, collider):
    #    if collider == 'blue_monster':
    #        if pygame.sprite.collide_rect(self, collider):
    #            if collider.direction == RIGHT:
    #                #print 'moving right'
    #                collider.direction = LEFT
    #            else:
    #                collider.direction = RIGHT
        
        
#class HorizontalWall(pygame.sprite.Sprite):
#    def __init__(self, start_position):
#        pygame.sprite.Sprite.__init__(self)
#        self.image = pygame.image.load(os.path.join(DIR, 'wall_h.png'))
#        self.rect = self.image.get_rect()
#        self.rect.topleft = start_position
#        self.bottom = self.rect.bottom
#        self.top = self.rect.top
#
#class VerticalWall(pygame.sprite.Sprite):
#    def __init__(self, start_position):
#        pygame.sprite.Sprite.__init__(self)
#        self.image = pygame.image.load(os.path.join(DIR, 'wall_v.png'))
#        self.rect = self.image.get_rect()
#        self.rect.topleft = start_position
#        self.bottom = self.rect.bottom
#        self.top = self.rect.top
#        self.right = self.rect.right
#        self.left = self.rect.left
