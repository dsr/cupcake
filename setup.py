"""
This is a setup.py script generated by py2applet

Usage:
    python setup.py py2app
"""

from setuptools import setup
APP = ['Cupcake.py']
DATA_FILES = ['player.png', 'monster_blue.png', 'monster_red.png','reward.png', 'reward_bite1.png', 'reward_bite2.png', 'bkgrnd.png', 'pacdies.ogg', 'chomp.ogg']
#OPTIONS = {'argv_emulation': True, }

setup(
    app=APP,
    data_files=DATA_FILES,
    options=
            dict(py2app=dict(
            iconfile =  'cupcake.icns',
            plist=dict(
                Copyright='2010 Daniel Ross',
                NSHumanReadableCopyright='2010 Daniel Ross',
                CFBundleVersion='1.6',
            ),
    )),
    setup_requires=['py2app'],
)
