#!/usr/bin/env python

### included __future__ imports to get used to python 3 conventions ###
from __future__ import print_function
from __future__ import unicode_literals
import os
import sys
import pygame
from random import randint
from pygame.locals import *
import helpers
import sqlite3
#import string
#import time
import player
import monsters
import reward


pygame.mixer.pre_init(44100, -16, 2, 2048)
pygame.init()
os.environ['SDL_VIDEO_CENTERED'] = '1'

#### Constants ###
DB_DIR = helpers.get_app_support_dir()
DB_PATH = DB_DIR + "Cupcake.db"
EXEC_DIR = os.path.dirname(__file__)
DOWN = 7
RIGHT = 5
UP = 3
LEFT = 9
START = [50,50]
DIR = os.path.dirname(__file__)
SCREEN_SIZE = [480, 480]

### Connect and/or create databse ###
DB = helpers.db_connect('CREATE TABLE IF NOT EXISTS cupcake_data \
    (id INTEGER PRIMARY KEY, name VARCHAR(50) , score VARCHAR(50), time VARCHAR(50))', DB_DIR, 'Cupcake.db')
    

### Load files ###
bkgrnd = pygame.image.load(os.path.join(EXEC_DIR,'bkgrnd.png'))
icon = pygame.image.load(os.path.join(EXEC_DIR,'reward.png'))

#### Other globals ####
difficulty = 1
bites = 0
time_alive = 0
eaten_cupcakes = 0
reward_pos = [randint(20, 400), randint(30, 400)]
font = pygame.font.SysFont('Helvetica', 25)
small_font = pygame.font.SysFont('Helvetica', 20)
big_font = pygame.font.SysFont('Helvetica', 65)
game_over = big_font.render("Game Over", 1, (255,10,10))
press_spacebar = font.render('Press spacebar to play again', 1, (255, 10, 10))
clock = pygame.time.Clock()

### These are state handlers ###
running = False
getting_name = True
### Set icons and caption ###
pygame.display.set_icon(icon)
pygame.display.set_caption('Cupcake')

def show_top_score():
    """Show top 5 scores ordered by number of cupcakes eaten"""
    connect = sqlite3.connect(DB_PATH)
    cursor = connect.cursor()
    results = cursor.execute('SELECT name, score, time FROM cupcake_data ORDER BY score DESC LIMIT 5')
    y = 75
    top_score = font.render("Top Scores", 1, [0, 0, 0])
    screen.blit(top_score, [155, 45])
    for row in results:
        secs = int(row[2])/1000
        n = str(row[0])
        e = str(row[1])
        t = str(row[2])
        x = font.render("Name: %s, Eaten: %s, Time: %s sec." % (n, e, secs), 1, [0, 0, 0])
        screen.blit(x, [35,y])
        y += 20

def record_data(name, cupcakes, time):
    """Inserts data into the sqlite database"""
    data = (name, cupcakes, time)
    connect = sqlite3.connect(DB_PATH)
    cursor = connect.cursor()
    cursor.execute('INSERT INTO cupcake_data VALUES (null, ?, ?, ?)', data)
    connect.commit()
    cursor.close()

def play_sound(path_to_sound):
    """Play sound"""
    pygame.mixer.music.load(path_to_sound)
    pygame.mixer.music.play()

for x, y in [SCREEN_SIZE]:
        screen = pygame.display.set_mode((x, y))

### Create the player and reward ###
player = player.Player(START)
reward = reward.Reward(reward_pos)

### Groups of sprites ###
blue_monsters = pygame.sprite.Group()
red_monsters = pygame.sprite.Group()
reward_group = pygame.sprite.GroupSingle()
player_group = pygame.sprite.GroupSingle()

### Coords for blue monsters
def add_blue_guys():
    num_blue = randint(difficulty - 1, difficulty)
    for b in range(num_blue):
        for x, y in [[randint(10, 450), randint(10, 450)]]:
            blue_monsters.add(monsters.Monster([x,y], 'monster_blue', RIGHT))

### Coords for red monsters ###
def add_red_guys():
    num_red = randint(difficulty - 1, difficulty)
    for b in range(num_red):
        for x, y in [[randint(10, 450), randint(10, 450)]]:
            red_monsters.add(monsters.Monster([x, y], 'monster_red', UP))

def add_player():
    player_group.add(player)

def reset_monsters():
    red_monsters.empty()
    blue_monsters.empty()
    add_blue_guys()
    add_red_guys()

def add_monsters_players():
    add_blue_guys()
    add_red_guys()
    add_player()

### Add blue, red monsters and player for initial game
add_monsters_players()
reward_group.add(reward)

### Create list for player name to be created from ###
entered_name = []

### Get the player name ###
def main():
    global eaten_cupcakes
    global difficulty
    global getting_name
    while getting_name:
        mods = pygame.key.get_mods()
        x = 380
        key = pygame.key.get_pressed()
        pygame.key.set_repeat(0,0)
        #entered_name = []
        screen.fill([255,255,255])
        #key_press = pygame.key.get_pressed()
        enter_name = small_font.render('Please enter your name and press enter:', 1, (255, 10, 10))
        screen.blit(enter_name, [10, 100])
        
        if key[pygame.K_ESCAPE]:
            sys.exit()
        elif (mods & KMOD_META):
            if key[pygame.K_q]:
                sys.exit()
        elif key[pygame.K_RETURN]:
            getting_name = False
            #showing_name = True
            running = True
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                    sys.exit()
            if event.type == pygame.KEYDOWN:
                if key[pygame.K_RETURN]:
                    pass    
                else:
                    key_value = pygame.key.name(event.key)
                    if key_value == 'backspace':
                        if entered_name:
                            entered_name.pop()
                            #print entered_name
                    if not key_value == 'backspace':
                        entered_name.append(key_value)
                    
        if 'return' in entered_name:
            entered_name.pop()
        for letter in entered_name:
            if not letter == 'backspace':
                x_letter = small_font.render(letter, 1, (255, 10, 10))
                screen.blit(x_letter, [x, 100])
                letter_size = small_font.size(letter)
                x += letter_size[0]
                
        pygame.display.update()
    
    ### Prepare player name ###
    player_name = "".join(entered_name)
    my_clock = pygame.time.Clock()
    my_time = 0
    ### Main game loop ###
    pygame.key.set_repeat(5,5) ## reset key repeat so holding down the key moves the character
    while running:
        #print my_clock.tick()
        my_time += my_clock.tick()
        #print my_clock.tick()
        #print my_time
        mods = pygame.key.get_mods()
        key = pygame.key.get_pressed()
        clock.tick(20) #this is here to slow things down to reduce cpu usage
        screen.fill([255,255,255])
        #screen.blit(bkgrnd, bkgrnd.get_rect())
        screen.blit(bkgrnd, [0, 30])
        text = font.render("Lives: " + str(player.lives), 1, (100, 0, 100))
        cupcake_text = font.render("Cupcakes: " + str(eaten_cupcakes), 1, (100, 0, 100))
        screen.blit(cupcake_text, [175, 0])
        screen.blit(text, [5, 0])                                                      
    
        if key[pygame.K_ESCAPE]:
            #print my_time
            sys.exit()
        elif (mods & KMOD_META):
            if key[pygame.K_q]:
                sys.exit()
        else:
            pass
             
        #### Draw monsters ####
        blue_monsters.draw(screen)
        red_monsters.draw(screen)
        reward_group.draw(screen)
        player_group.draw(screen)
        blue_monsters.update('horizontal', SCREEN_SIZE[0])
        red_monsters.update('vertical', SCREEN_SIZE[1])
    
        ### Check for collisions with monsters ###
        for monster in blue_monsters:
            player.check_collision(monster, 'blue_monsters')
    
        for monster in red_monsters:
            player.check_collision(monster, 'red_monsters')
    
    
        #### Keep track of bites and modify reward_group ####
        if reward.bites == 0:
            for x in reward_group:
                if player.check_collision(x, 'reward_group'):
                    reward.bites = 1
                    reward_group.update()
        elif reward.bites == 1:
            for x in reward_group:
                if player.check_collision(x, 'reward_group'):
                    reward.bites = 2
                    reward_group.update()
        elif reward.bites == 2:
            for x in reward_group:
                if player.check_collision(x, 'reward_group'):
                    reward.bites = 3
                    reward_group.update()
    
        ### Handle player dying ###
        if player.lives == 0:
    
            if eaten_cupcakes:
                time_alive = pygame.time.get_ticks()
                record_data(player_name, eaten_cupcakes, my_time)
                eaten_cupcakes = 0
            screen.blit(game_over, [80,240])
            screen.blit(press_spacebar, [80, 320])
            show_top_score()
    
            blue_monsters.empty()
            red_monsters.empty()
            player_group.empty()
        
        #### Begin handling events ####
        for event in pygame.event.get():
            player.update(key, SCREEN_SIZE)
           
            if event.type == pygame.QUIT:
                running = False
            if reward.bites == 3:
                eaten_cupcakes += 1
                reward.bites = 0
                difficulty += 1
                reset_monsters()
            if player.lives == 0:
                if key[pygame.K_SPACE]:
                    pygame.event.clear()
                    #record_data(player_name, eaten_cupcakes, time_alive)
                    #pygame.event.set_blocked([KEYDOWN, KEYUP])
                    eaten_cupcakes = 0
                    my_time = 0
                    player.lives = 3
                    difficulty = 1
                    add_monsters_players()
        pygame.display.update()

if __name__ == '__main__':
    main()
