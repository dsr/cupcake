import pygame
from pygame.locals import *
import sys
import os

#print __file__
DIR = os.path.dirname(__file__)
EXEC_DIR = os.path.dirname(__file__)

class Player(pygame.sprite.Sprite):
    """ This is our main self class """
    image = None
    def __init__(self, initial_position):
        pygame.sprite.Sprite.__init__(self)
        if Player.image == None:
            Player.image = pygame.image.load('player.png')
        self.initial_position = initial_position
        self.image = Player.image
        self.rect = self.image.get_rect()
        self.rect.topleft = initial_position
        self.next_update_time = 0
        self.bottom = self.rect.bottom
        self.top = self.rect.top
        self.right = self.rect.right
        self.left = self.rect.left
        self.direction = None
        self.lives = 3
        self.chomp_sound = pygame.mixer.Sound('chomp.ogg')
        self.died_sound = pygame.mixer.Sound('pacdies.ogg')

    def update(self, key, screen_size):
        """Update according to keypresses and update the rect positions"""
        self.top = self.rect.top
        self.bottom = self.rect.bottom
        self.right = self.rect.right
        self.left = self.rect.left
        if key[pygame.K_DOWN]:
            self.direction = 'down'
            if self.rect.bottom < screen_size[1]:
                self.move(0, 5)
            else:
                pass
        elif key[pygame.K_UP]:
            self.direction = 'up'
            if self.rect.top > 30:
                self.move(0, -5)
            else:
                pass
        elif key[pygame.K_RIGHT]:
            self.direction = 'right'
            if self.rect.right < screen_size[0]:
                self.move(5, 0)
            else:
                pass
        elif key[pygame.K_LEFT]:
            self.direction = 'left'
            if self.rect.left > 0:
                self.move(-5, 0)
            else:
                pass

    def move(self, x, y):
        self.rect.x += x
        self.rect.y += y

    def check_collision(self, collider, group):
        """Check if there's a collision with a monster or bad guy"""
        if ((self.rect.centerx <= (collider.rect.centerx + collider.rect.width/2)) and \
            self.rect.centerx >= (collider.rect.centerx - collider.rect.width/2)) and \
                ((self.rect.centery <= (collider.rect.centery + collider.rect.height/2)) and \
                 self.rect.centery >= (collider.rect.centery - collider.rect.height/2)):
                if group == "reward_group":
                    self.chomp_sound.play()
                    #play_sound('chomp.ogg')
                    if self.direction == 'down':
                        self.move(0, -35)
                    if self.direction == 'up':
                        self.move(0, 35)
                    if self.direction == 'right':
                        self.move(-35, 0)
                    if self.direction == 'left':
                        self.move(35, 0)
                elif group == "blue_monsters" or "red_monsters":
                    self.rect.topleft = self.initial_position 
                    self.lives -= 1
                    if self.lives == 0:
                        self.died_sound.play()
                        #play_sound('pacdies.ogg')

                return True
