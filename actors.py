import pygame
#from pygame.locals import *
#import sys
import os

UP = 3
DOWN = 7
RIGHT = 5
LEFT = 9
#print __file__
DIR = os.path.dirname(__file__)

class Player(pygame.sprite.Sprite):
    def __init__(self, initial_position):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load('player.png')
        self.rect = self.image.get_rect()
        self.rect.topleft = initial_position
        self.next_update_time = 0
        self.bottom = self.rect.bottom
        self.top = self.rect.top
        self.right = self.rect.right
        self.left = self.rect.left

    def update(self):
        """Update according to keypresses and update the rect positions"""
        self.top = self.rect.top
        self.bottom = self.rect.bottom
        self.right = self.rect.right
        self.left = self.rect.left
        
    def move(self, x, y):
        self.rect.x += x
        self.rect.y += y
#        
    
    def check_collision(self, collider, new_location):
        ### TODO: Look at pygame.sprite.collide_circle_ratio or pygame.sprite.collide_rect_ration to replace this convoluted mess
        """Check if there's a collision with a monster or bad guy"""
        if ((self.rect.centerx <= (collider.rect.centerx + collider.rect.width/2)) and \
            self.rect.centerx >= (collider.rect.centerx - collider.rect.width/2)) and \
            ((self.rect.centery <= (collider.rect.centery + collider.rect.height/2)) and \
             self.rect.centery >= (collider.rect.centery - collider.rect.height/2)):
            self.rect.topleft = new_location
            return True        
        
class Monster(pygame.sprite.Sprite):
    def __init__(self, initial_position, type, direction):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load(type + '.png')
        #self.image = pygame.image.load('circles.png')
        self.rect = self.image.get_rect()
        self.rect.topleft = initial_position
        self.next_update_time = 0
        self.bottom = self.rect.bottom
        self.top = self.rect.top
        self.right = self.rect.right
        self.left = self.rect.left
        self.direction = direction
        self.type = type

    def update(self):
        self.top = self.rect.top
        self.left = self.rect.left
        self.right = self.rect.right
        self.bottom = self.rect.bottom

    def move(self, plane, bounds):
        if plane == 'horizontal':
            if self.direction == RIGHT:
                self.rect.left += 1
                if self.right > bounds:
                    self.reverse()
            elif self.direction == LEFT:
                self.rect.left -= 1
                if self.left < 0:
                    self.reverse()
        elif plane == 'vertical':
            if self.direction == UP:
                self.rect.top -= 1
                if self.top < 0:
                    self.reverse()
            elif self.direction == DOWN:
                self.rect.top += 1
                if self.bottom > bounds:
                    self.reverse()
                
                
    def reverse(self):
        if self.direction == RIGHT:
            self.direction = LEFT
        elif self.direction == LEFT:
            self.direction = RIGHT
        elif self.direction == UP:
            self.direction = DOWN
        elif self.direction == DOWN:
            self.direction = UP
    
    
    #def wall_collision(self, wall_group):
    #    if self.type == 'monster_blue':
    #        for sprite in wall_group.sprites():
    #            if pygame.sprite.collide_rect(sprite, self):
    #               if self.direction == RIGHT:
    #                   #print 'moving right'
    #                    self.direction = LEFT
    #               elif self.direction == LEFT:
    #                   # print 'moving left, collided'
    #                    self.direction = RIGHT
    #                    #print self.direction
    #
    #    if self.type == 'monster_red':
    #        for sprite in wall_group.sprites():
    #            if pygame.sprite.collide_rect(sprite, self):
    #          
    #                if self.direction == UP:
    #                    self.direction = DOWN
    #                else:
    
    #                    self.direction = UP
    

class Wall(pygame.sprite.Sprite):
    def __init__(self, start_position, type):
        pygame.sprite.Sprite.__init__(self)
        if type == 'horizontal':
            self.image = pygame.image.load('wall_h.png')
        elif type == 'vertical':
            self.image = pygame.image.load('wall_v.png')
        
        self.rect = self.image.get_rect()
        self.rect.topleft = start_position
        self.bottom = self.rect.bottom
        self.top = self.rect.top
        self.right = self.rect.right
        self.left = self.rect.left
        
class Reward(pygame.sprite.Sprite):
    def __init__(self, image_file, position):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load(image_file)
        self.rect = self.image.get_rect()
        self.rect.topleft = position
